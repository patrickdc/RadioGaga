<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="description" content="Radio Patrick. - Enjoy The Greatest Hits">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title>Radio Patrick</title>
</head>

<body>

	<video autoplay loop id="video-background" muted plays-inline>
		<source src="videos/background.mp4" type="video/mp4">
	</video>

	<header id="header">
			<div id="logo">
				Radio Patrick
			</div>

			<nav id="main-nav">
				<ul>
					<li><a href="index.php">Home</a></li>
					<li><a href=playlist.php>Playlist</a></li>
				</ul>
			</nav>
	</header>

	<div id="indexDiv">
		<h1>Welcome to Radio Patrick</h1>
		<br>
		<h2>Enjoy the Greatest Music</h2>
		<br>
		<a href="playlist.php">Click here for the playlist</a>
	</div>

</body>


</html>