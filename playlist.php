<?php

if(!isset($_GET['albumName']))
{
	$albumName = "Awesome Mix Vol 2";
}
else
{
	$albumName = $_GET['albumName'];
}

$albums  = array
(
	"PATRICK" => array
	(
		"Awesome Mix Vol 2" => array
		(
			"image" => "images\album1.jpg",
			"youtubeURL" => "0LSVta2GJpM",
			"tracks" => array
			(
				array
				(
					"songTitle" 	=> "Lake Shore Drive",
					"songFile"		=> "LakeShoreDrive.mp3",
					"songDuration"	=> "4.53",
				),
				array
				(
					"songTitle" 	=> "Brandy (You're A Fine Girl)",
					"songFile"		=> "Brandy.mp3",
					"songDuration"	=> "3.10",
				),
				array
				(
					"songTitle" 	=> "Come And Get Your Love",
					"songFile"		=> "ComeGetYourLove.mp3",
					"songDuration"	=> "3.27",
				)
			)
		),

		"Nintendo" => array
		(
			"image" => "images\album2.jpg",
			"youtubeURL" => "AH8uNvP9iOE",
			"tracks" => array
			(
				array
				(
					"songTitle" 	=> "Jump Up",
					"songFile"		=> "JumpUp.mp3",
					"songDuration"	=> "4.06",
				),
				array
				(
					"songTitle" 	=> "Dire Dire Docks",
					"songFile"		=> "DireDireDocks.mp3",
					"songDuration"	=> "6.05",
				),
				array
				(
					"songTitle" 	=> "Throwback Galaxy",
					"songFile"		=> "ThrowbackGalaxy.mp3",
					"songDuration"	=> "2.32",
				)
			)
		),

		"Guitar Hero" => array
		(
			"image" => "images\album3.jpg",
			"youtubeURL" => "Y-h7VGAa5r4",
			"tracks" => array
			(
				array
				(
					"songTitle" 	=> "Carry On My Wayward Son",
					"songFile"		=> "Wayward.mp3",
					"songDuration"	=> "5.23",
				),
				array
				(
					"songTitle" 	=> "Free Bird",
					"songFile"		=> "FreeBird.mp3",
					"songDuration"	=> "8.52",
				),
				array
				(
					"songTitle" 	=> "More Than A Feeling",
					"songFile"		=> "MoreThanAFeeling.mp3",
					"songDuration"	=> "4.46",
				)
			)
		)
	)
);
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta name="description" content="Radio Patrick. - Enjoy The Greatest Hits">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title>Radio Patrick</title>
</head>



<body>

	<header id="header">
		<div id="logo">
			Radio Patrick
		</div>

		<nav id="main-nav">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="playlist.php">Playlist</a></li>
			</ul>
		</nav>
	</header>

	<video autoplay loop id="video-background" muted plays-inline>
  		<source src="videos/background.mp4" type="video/mp4">
	</video>

	<div id="mainPlayDiv">


		<div id="playlistDiv">

			<?php echo "<h1>$albumName</h1>" ?>

			<iframe width="532" height="400" src="https://www.youtube.com/embed/<?php echo $albums["PATRICK"][$albumName]["youtubeURL"]; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>


			<br>

			<audio id="audioPlayer" src="" controls="controls" align=""> </audio>

			<table>
				<thead>
					<tr>
						<th>TRACK</th>
						<th>TITLE</th>
						<th>TIME</th>
						<th>PLAY</th>
					</tr>
				</thead>

				<script>
					function sendSong(songPath)
					{
						let audioPlayer = document.getElementById("audioPlayer");
						audioPlayer.src = "songs/" + songPath;
						audioPlayer.load();
						audioPlayer.play();
					}
				</script>
				
				<tbody>
					<?php
						$trackID = 1;
							foreach($albums["PATRICK"]["$albumName"]["tracks"] as $trackArray)
							{
								echo "<tr>";
									echo "<td>"; echo "$trackID"; echo "</td>"; //track IDs on left side of the table
									echo "<td>"; echo $trackArray["songTitle"]; echo "</td>";
									echo "<td>"; echo $trackArray["songDuration"]; echo "</td>";
									echo "<td>"; echo "<button id=\"playButton\" onclick='sendSong(\"" . $trackArray["songFile"] . "\");'>Play</button>"; echo "</td>";
								echo "</tr>";
							$trackID++;
							};
					?>
					
				</tbody>
			</table>

			<br>

		</div>


		<div id="albumDiv">
			<div class="albumButtonDiv">
				<a onclick="window.location.href = '?albumName=Awesome%20Mix%20Vol%202'"><img src="images/album1.jpg"></a>
			</div>
			<div class="albumButtonDiv">
				<a onclick="window.location.href = '?albumName=Nintendo'"><img src="images/album2.jpg"></a>
			</div>
			<div class="albumButtonDiv">
				<a onclick="window.location.href = '?albumName=Guitar%20Hero'"><img src="images/album3.jpg"></a>
			</div>
		</div>
	</div>

	<div id="cleardiv">
	</div>

</body>

</html>